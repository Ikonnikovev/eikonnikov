package Lab1;

/**
 * Created by Admin on 08.10.14.
 */
public class ThermalPowerPlant extends PowerPlant {
    private double annualAmountOfConsumedFuel;
    private double amountOfFuelForOnekWh;

    public ThermalPowerPlant() {

    }

    public ThermalPowerPlant(String name, int electricalPower, int yearOfFoundation,
                             String location, double annualAmountOfConsumedFuel, double amountOfFuelForOnekWh) {
        super(name, electricalPower, yearOfFoundation, location);
        this.annualAmountOfConsumedFuel = annualAmountOfConsumedFuel;
        this.amountOfFuelForOnekWh = amountOfFuelForOnekWh;
    }

    public void setAnnualAmountOfConsumedFuel(double annualAmountOfConsumedFuel) {
        this.annualAmountOfConsumedFuel = annualAmountOfConsumedFuel;
    }

   @Override
    public void setElectricalPower(int electricalPower) {
        if (electricalPower > 1000){
            this.electricalPower = 1000;
        } else {
			this.electricalPower = electricalPower;	
        }
    }



    public void setAmountOfFuelForOnekWh(double amountOfFuelForOnekWh) {
        this.amountOfFuelForOnekWh = amountOfFuelForOnekWh;
    }

    public double getAnnualAmountOfConsumedFuel() {
        return annualAmountOfConsumedFuel;
    }

    public double getAmountOfFuelForOnekWh() {
        return amountOfFuelForOnekWh;
    }

    @Override
    public String toString() {
        return super.toString() + "Годовое потребление топлива составляет " + this.annualAmountOfConsumedFuel
                + " нм3 природного газа. \nУдельный расход топлива составляет " + this.amountOfFuelForOnekWh + " кг/(кВт*ч).\n";
    }

    public void showPrincipalTypeOfEquipment() {
        System.out.println(super.getName() + " - основным оборудованием на теловой " +
                "электростанции является энергетический котел и паровая турбина.\n");
    }

    public void showAnnualGrossElectricalOutput() {
        System.out.println(super.getName() + " вырабатывает в год "
                + annualAmountOfConsumedFuel / amountOfFuelForOnekWh + " кВт*ч электроэнергии.\n");
    }
    //Overload
    public void showAnnualGrossElectricalOutput(int year) {
        System.out.println(super.getName() + " вырабатывает за " + year + " лет "+
                ((annualAmountOfConsumedFuel / amountOfFuelForOnekWh)*year) + " кВт*ч электроэнергии.\n");
    }
}
