package Lab6_ListSort;

import java.util.*;
import java.lang.Math.*;

public class ListSortTest {
    public static void main(String[] args) {

        ArrayList<Integer> listOfInteger = new ArrayList<Integer>() {
        };
        listOfInteger.add(1);
        listOfInteger.add(-2);
        listOfInteger.add(33);
        listOfInteger.add(-36);
        listOfInteger.add(38);

        System.out.println(listOfInteger);

        Collections.sort(listOfInteger);

        System.out.println(listOfInteger);

        Collections.sort(listOfInteger, new Comparator<Integer>() {

            public int compare(Integer firstObject, Integer secondObject) {
                return Math.abs(firstObject % 33) - Math.abs(secondObject % 33);
            }
        });

        System.out.println(listOfInteger);

    }
}
