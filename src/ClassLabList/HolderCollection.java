package ClassLabList;
import java.util.Iterator;

public class HolderCollection implements Iterable {

    public Holder headReference = null;
    public int size = 0;

    public void add(Integer data) {
        if (size == 0) {
            headReference = new Holder(data);
        } else {
            Holder temporaryReference = headReference;
            for (int i = 0; i < (size - 1); i++) {
                temporaryReference = temporaryReference.next;
            }
            temporaryReference.next = new Holder(data);
        }
        size++;
    }

    public void addByIndex(Integer data, int index) {
        if (index>size || index<0){
            System.out.println("Inappropriate index!");
        } else if (index==size){
            add(data);
        } else {
            if (index == 0) {
                Holder temporaryReference = headReference;
                headReference = new Holder(data);
                headReference.next=temporaryReference;
            } else {
                Holder temporaryReference = headReference;
                for (int i = 0; i < index-1; i++) {
                    temporaryReference = temporaryReference.next;
                }
                Holder temporaryReferenceForNext = temporaryReference.next;
                temporaryReference.next = new Holder(data);
                temporaryReference.next.next=temporaryReferenceForNext;
            }
            size++;
        }


    }

    public void remove (){
        Holder temporaryReference = headReference;
        for (int i = 0; i < (size - 2); i++) {
            temporaryReference = temporaryReference.next;
        }
        temporaryReference.next = null;
        size--;
    }

    public void removeByIndex (int index){
        if (index>=size || index<0){
            System.out.println("Inappropriate index!");
        } else if (index==size-1){
            remove();
        } else {
            if (index == 0) {
                headReference = headReference.next;
            } else {
                Holder temporaryReference = headReference;
                for (int i = 0; i < index-1; i++) {
                    temporaryReference = temporaryReference.next;
                }
                Holder temporaryReferenceForNext = temporaryReference.next.next;
                temporaryReference.next = temporaryReferenceForNext;

            }
            size--;
        }
    }

    public Integer getData(){
        return getDataByIndex(0);
    }

    public Integer getDataByIndex(int index) {
        Holder temporaryReference = headReference;
        if (index>=size || index<0){
            System.out.println("Inappropriate index!");
        } else {
            for (int i = 0; i < size; i++) {
                if (index == i) {
                    return temporaryReference.data;
                } else {
                    temporaryReference = temporaryReference.next;
                }
            }
        }
        return null;
    }


    public Iterator iterator() {
        return new HolderCollectionIterator();
    }

    private class HolderCollectionIterator implements Iterator {
        int currentPosition;
        @Override
        public boolean hasNext() {
            return currentPosition < size;
        }

        @Override
        public Integer next() {
            return HolderCollection.this.getDataByIndex(currentPosition++);
        }

        @Override
        public void remove() {

        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
