package LevelUp.Model;

/**
 * Created by user on 16.10.14.
 */
public interface Movable {
    void move (double x, double y);
    void stop ();
}
