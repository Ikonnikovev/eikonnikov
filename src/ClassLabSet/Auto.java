package ClassLabSet;

/**
 * Created by user on 06.11.14.
 */
public class Auto {
    public String name;
    public String color;
    public String number;

    public Auto(String name, String color, String number) {
        this.name = name;
        this.color = color;
        this.number = number;
    }

    public String toString() {
        return "{"+color + ", " + number + ", " + name+"}";
    }
}
