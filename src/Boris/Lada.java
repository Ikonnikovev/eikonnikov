package Boris;
public class Lada extends Car{

    @Applicable(parts = {"210", "211", "212"})
    Body body;
    @Applicable(parts = {"220", "221", "222"})
    Engine engine;
    @Applicable(parts = {"230", "231", "232"})
    Transmission transmission;

    public Lada() {
    }

    public Lada(Body body, Engine engine, Transmission transmission) {
        this.body = body;
        this.engine = engine;
        this.transmission = transmission;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public String toString(){
        return body.code + transmission.code + engine.code;
    }
}
