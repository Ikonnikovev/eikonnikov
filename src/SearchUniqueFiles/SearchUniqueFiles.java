package SearchUniqueFiles;

import java.io.File;
import java.util.TreeSet;

/**
 * Created by user on 18.11.14.
 */
public class SearchUniqueFiles {
//    private static TreeSet<File> mySet = new TreeSet<File>(new Comparator<File>() {
//        @Override
//        public int compare(File f1, File f2) {
//
//            if (f1.getName().equals(f2.getName())){
//                return 0;
//            }
//            return 1;
//        }
//    } );

    private static TreeSet<String> mySet = new TreeSet<String>();

    public static void main(String[] args) {
        File directory = new File ("D:/Result");
        searchFiles(directory);
        System.out.println(mySet);

    }

    public static void searchFiles (File directoryForSearch){
        File[] list = directoryForSearch.listFiles();
        for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
                searchFiles(list[i]);
            } else {
//                mySet.add(list[i]);
                mySet.add(list[i].getName());
                System.out.println(list[i].getName());

            }
        }

    }
}
