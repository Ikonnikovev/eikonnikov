package Boris;
public class Opel extends  Car {
    @Applicable(parts = {"110","111","112"})
    Body body;
    @Applicable(parts = {"120","121","122"})
    Engine engine;
    @Applicable(parts = {"130","131","132"})
    Transmission transmission;

    public Opel() {
    }

    public Opel(Body body, Engine engine, Transmission transmission) {
        this.body = body;
        this.engine = engine;
        this.transmission = transmission;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }
    public String toString(){
        return body.code + transmission.code + engine.code;
    }
}
