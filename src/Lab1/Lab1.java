package Lab1;

/**
 * Created by Admin on 08.10.14.
 */
public class Lab1 {
    public static void main(String[] args) {
        ThermalPowerPlant LuganskayaTEC = new ThermalPowerPlant();
        LuganskayaTEC.setName("Луганская ТЭЦ");
        LuganskayaTEC.setElectricalPower(1200);
        LuganskayaTEC.setYearOfFoundation(1976);
        LuganskayaTEC.setLocation("Луганск");
        LuganskayaTEC.setAnnualAmountOfConsumedFuel(1400000);
        LuganskayaTEC.setAmountOfFuelForOnekWh(0.8);

        ThermalPowerPlant KyevskayaTEC = new ThermalPowerPlant("Киевская ТЭЦ", 2400, 1974, "Киев", 1000000, 0.5);

        HydroPowerPlant DnistovskayaHPP = new HydroPowerPlant();
        DnistovskayaHPP.setName("Днестровская ГЭС");
        DnistovskayaHPP.setElectricalPower(700);
        DnistovskayaHPP.setYearOfFoundation(1972);
        DnistovskayaHPP.setLocation("Новоднестровск");
        DnistovskayaHPP.setAnnualAmountOfConsumedWater(700000);
        DnistovskayaHPP.setAmountOfWaterForOnekWh(0.07);

        HydroPowerPlant DniproHPP = new HydroPowerPlant("ДнепроГЭС", 1500, 1934, "Запорожье", 2000000, 0.05);

        System.out.println(LuganskayaTEC);
        System.out.println(KyevskayaTEC);
        System.out.println(DnistovskayaHPP);
        System.out.println(DniproHPP);

        LuganskayaTEC.showPrincipalTypeOfEquipment();
        KyevskayaTEC.showPrincipalTypeOfEquipment();
        DnistovskayaHPP.showPrincipalTypeOfEquipment();
        DniproHPP.showPrincipalTypeOfEquipment();

        LuganskayaTEC.showAnnualGrossElectricalOutput();
        LuganskayaTEC.showAnnualGrossElectricalOutput(10);
        KyevskayaTEC.showAnnualGrossElectricalOutput();
        DnistovskayaHPP.showAnnualGrossElectricalOutput();
        DniproHPP.showAnnualGrossElectricalOutput();

    }
}
