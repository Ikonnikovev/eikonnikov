package ReflectionTest;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionTest {
    public static void main(String[] args) {
        doReflection("ReflectionTest.BookReader", "ReflectionTest.Book");
    }

    public static void doReflection(String o1, String o2) {
        Class readerClass = null;
        Class bookClass = null;
        try {
            readerClass = Class.forName(o1);
            bookClass = Class.forName(o2);
            Object myReader = readerClass.newInstance();
            Object myBook = bookClass.newInstance();

            Field bookAuthor = bookClass.getDeclaredField("author");
            Field bookName = bookClass.getDeclaredField("name");
            bookAuthor.set(myBook, "Pushkin");
            bookName.set(myBook, "Onegin");

            Field readerFirstName = readerClass.getDeclaredField("firstName");
            Field readerLastName = readerClass.getDeclaredField("lastName");
            Field readerAge = readerClass.getDeclaredField("age");
            readerFirstName.set(myReader, "Ivan");
            readerLastName.set(myReader, "Ivanov");
            readerAge.set(myReader, 50);

            Method readBook = readerClass.getDeclaredMethod("readBook", String.class, String.class);
            readBook.invoke(myReader, (String) bookAuthor.get(myBook), (String) bookName.get(myBook));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }


    }
}
