package LevelUp.Model;

/**
 * Created by user on 16.10.14.
 */
public abstract class Vehicle implements Changable, Movable, Refuelable {
    private int height;
    private int width;
    private int length;
    private double speed;
    private int wheelCount;

    protected Vehicle() {
    }

    protected Vehicle(int height, int width, int length, double speed, int wheelCount) {
        this.height = height;
        this.width = width;
        this.length = length;
        this.speed = speed;
        this.wheelCount = wheelCount;
    }

    public abstract void aging (int years);

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getWheelCount() {
        return wheelCount;
    }

    public void setWheelCount(int wheelCount) {
        this.wheelCount = wheelCount;
    }
}
