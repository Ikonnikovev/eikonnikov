package CarConveyor;

/**
 * Created by user on 25.11.14.
 */
public class CarZaz {
    @Applyable(parts = {"A1A1"})
    public String gearBox;
    @Applyable(parts = {"CC1"})
    public String engine;
    @Applyable(parts = {"E2"})
    public String body;

    @Override
    public String toString (){
        return "Zaz gearBox = "+gearBox;
    }
}
