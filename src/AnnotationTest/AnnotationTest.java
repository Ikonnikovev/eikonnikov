package AnnotationTest;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class AnnotationTest {
    public static void main(String[] args) {
        HashMap<String, Object> fields = new HashMap<String, Object>();
        fields.put("name", "Henric");
        fields.put("age", 50);
        fields.put("inn", "AE123");
        fields.put("active", true);
        AnnotationTest test = new AnnotationTest();
        test.fillFields(fields);
    }

    public void fillFields(HashMap<String, Object> fields) {
        try {
            Class user = Class.forName("AnnotationTest.User");
//            Class user = User.class;
            Object myUser = user.newInstance();
            Field[] userFields = user.getDeclaredFields();
            for (Field field: userFields){
                Object value = fields.get(field.getName());
                if ((value==null)&&checkNull(field)){
                    System.out.println("Cannot set null into field "+field.getName());
                    throw new RuntimeException("Cannot set null into field "+field.getName());
                }
                if (value!=null){
                    setValueIntoField (field.getName(), value, myUser);
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void setValueIntoField(String name, Object value, Object myUser) {
        String methodName = "set"+name.toUpperCase().charAt(0)+name.substring(1);
        try {
            Method method = myUser.getClass().getDeclaredMethod(methodName, value.getClass());
            method.invoke(myUser, value);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private boolean checkNull(Field field) {
        Nullable nullable = field.getAnnotation(Nullable.class);
        if (nullable!=null){
            if (nullable.value()==true){
                return true;
            }
        }
        return false;
    }
}
