package Lab1;

/**
 * Created by Admin on 08.10.14.
 */
public class HydroPowerPlant extends PowerPlant {
    private double annualAmountOfConsumedWater;
    private double amountOfWaterForOnekWh;

    public HydroPowerPlant() {

    }

    public HydroPowerPlant(String name, int electricalPower, int yearOfFoundation,
                           String location, double annualAmountOfConsumedWater, double amountOfWaterForOnekWh) {
        super(name, electricalPower, yearOfFoundation, location);
        this.annualAmountOfConsumedWater = annualAmountOfConsumedWater;
        this.amountOfWaterForOnekWh = amountOfWaterForOnekWh;
    }

    public void setAnnualAmountOfConsumedWater(double annualAmountOfConsumedWater) {
        this.annualAmountOfConsumedWater = annualAmountOfConsumedWater;
    }

    public void setAmountOfWaterForOnekWh(double amountOfWaterForOnekWh) {
        this.amountOfWaterForOnekWh = amountOfWaterForOnekWh;
    }

    public double getAnnualAmountOfConsumedWater() {
        return annualAmountOfConsumedWater;
    }

    public double getAmountOfWaterForOnekWh() {
        return amountOfWaterForOnekWh;
    }

    @Override
    public String toString() {
        return super.toString() + "Годовое потребление речной воды составляет " + this.annualAmountOfConsumedWater
                + " м3. \nУдельный расход воды составляет " + this.amountOfWaterForOnekWh + " м3/(кВт*ч).\n";
    }

    public void showPrincipalTypeOfEquipment() {
        System.out.println(super.getName() + " - основным оборудованием на гидроэлектростанции является гидравлическая турбина.\n");
    }

    public void showAnnualGrossElectricalOutput() {
        System.out.println(super.getName() + " вырабатывает в год "
                + annualAmountOfConsumedWater / amountOfWaterForOnekWh + " кВт*ч электроэнергии.\n");
    }
}
