package Lab4;

/**
 * Created by Admin on 22.10.14.
 */
public class Test {


    public static void printGreeting(Greeting greeting) {
        greeting.getGreeting();

    }

    public static void main(String[] args) {
        Greeting greetingInEnglish = new GreetingInEnglish();
        Greeting greetingInRussian = new GreetingInRussian("Имя");
        Greeting greetingInOneWord = new Greeting() {

            @Override
            public void getGreeting() {
                getGreetingWithName("SomethingWeird");
            }

            @Override
            public void getGreetingWithName(String name) {
                System.out.println("Hello " + name + " !");
            }
        };

        printGreeting(greetingInEnglish);
        printGreeting(greetingInRussian);
        printGreeting(greetingInOneWord);
        printGreeting(new Greeting() {
            @Override
            public void getGreeting() {
                getGreetingWithName("RealAnonymous");
            }

            @Override
            public void getGreetingWithName(String name) {
                System.out.println("Hello " + name + " !");
            }
        });

    }
}

