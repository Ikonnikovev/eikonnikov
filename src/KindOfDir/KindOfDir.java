package KindOfDir;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class KindOfDir {
    public static void main(String[] args) {
        myDir();
    }
    public static void myDir (){
        System.out.print("Please input a path:");
        Scanner sc = new Scanner(System.in);
        File file = new File(sc.nextLine());
        if (file.exists()){
            if (file.isDirectory()){
                ArrayList<String> listOfDirectory = new ArrayList<String>();
                ArrayList<String> listOfFile = new ArrayList<String>();
                File [] list = file.listFiles();
                for (File f: list){
                    if (f.isDirectory()){
                        listOfDirectory.add(f.getName());
                    } else {
                        if (!f.isHidden()) {
                            listOfFile.add(f.getName());
                        }
                    }
                }
                for (String d: listOfDirectory){
                    System.out.println(d+" - dir");
                }
                for (String f: listOfFile){
                    System.out.println(f);
                }
            } else {
                System.out.println("It is not a directory!");
            }
        } else {
            System.out.println("This path does not exist!");
        }
    }
}
