package Boris;
public class Transmission extends Part{
    public String code;

    public Transmission() {
    }

    public Transmission(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
