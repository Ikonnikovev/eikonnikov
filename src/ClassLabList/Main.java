package ClassLabList;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        HolderCollection holderCollection = new HolderCollection();
        holderCollection.add(0);
        holderCollection.add(1);
        holderCollection.add(2);
        holderCollection.add(3);
        holderCollection.add(4);

        System.out.println(holderCollection.getDataByIndex(2));
        System.out.println(holderCollection.getData());

        System.out.println("-------------");
        System.out.println(holderCollection.size);
        System.out.println("-------------");

        for (Object i : holderCollection) {
            System.out.println(i);
        }

        System.out.println("-------------");

        holderCollection.addByIndex(-1, 0);
        holderCollection.addByIndex(23, 4);
        holderCollection.addByIndex(5, 7);
        holderCollection.addByIndex(7, 9); //вызов сообщения о недопустимом индексе

        for (Object i : holderCollection) {
            System.out.println(i);
        }

        System.out.println("-------------");

        holderCollection.remove();
        holderCollection.removeByIndex(0);
        holderCollection.removeByIndex(3);
        holderCollection.removeByIndex(5);//вызов сообщения о недопустимом индексе


//        for (int i = 0; i < holderCollection.size; i++) {
//            System.out.println(holderCollection.getDataByIndex(i));
//        }

        for (Object i : holderCollection) {
            System.out.println(i);
        }
    }
}
