package Boris;
import java.util.ArrayList;

public class Bmw extends Car{
    @Applicable(parts = {"10","11","12"})
    public Body body;
    @Applicable(parts = {"20","21","22"})
    public Engine engine;
    @Applicable(parts = {"30","31","32"})
    public Transmission transmission;

    public Bmw() {
    }

    public Bmw(Body body, Engine engine, Transmission transmission) {
        this.body = body;
        this.engine = engine;
        this.transmission = transmission;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public String toString(){
        return body.code+" "+engine.code+" "+transmission.code;
    }
}
