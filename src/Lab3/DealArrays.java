package Lab3;

/**
 * Created by Admin on 12.10.14.
 */
public class DealArrays {
    public static void main(String[] args) {
        //Task 1
        String[] stringArray = createArray(5);
        for (int i = 0; i < stringArray.length; i++) {
            System.out.print(stringArray[i]);
        }
        System.out.println(" ");
        //Task 2
        int[] arr21 = {1, 2, 2};
        int[] arr22 = {4, 4, 1, 2, 2};
        System.out.println(either24(arr21));
        System.out.println(either24(arr22));

        //Task 3
        int[] arr31 = {1, 3, 2};
        int[] arr32 = {3, 1, 4, 5, 6};
        System.out.println(has12(arr31));
        System.out.println(has12(arr32));

        //Task 4
        int[] arr41 = {4, 7, 7, 3, 7, 7, 7, 5, 4, 4, 7, 7};
        int[] arr42 = {7, 7, 4, 7, 7, 1, 2, 5, 4, 5, 7};
        System.out.println(twoTwo(arr41));
        System.out.println(twoTwo(arr42));

        //Task 5
        int[] arrayOfNumbers = createArrayN(5, 10);
        for (int i = 0; i < arrayOfNumbers.length; i++) {
            System.out.print(arrayOfNumbers[i]);
        }
        System.out.println(" ");

        //Task 6
        int[] arr61 = {1, 2, 4, 1};
        int[] arr62 = {1, 4, 4, 4};
        int[] arr63 = {1, 2, 3, 1};
        int[] arrResult61 = pre4(arr61);
        for (int i = 0; i < arrResult61.length; i++) {
            System.out.print(arrResult61[i]);
        }
        System.out.println(" ");
        int[] arrResult62 = pre4(arr62);
        for (int i = 0; i < arrResult62.length; i++) {
            System.out.print(arrResult62[i]);
        }
        System.out.println(" ");
        int[] arrResult63 = pre4(arr63);
        for (int i = 0; i < arrResult63.length; i++) {
            System.out.print(arrResult63[i]);
        }
        System.out.println(" ");

        //Task 7
        int[] arr71 = {0, 2, 3, 1, 0, 0, 5, 6, 7, 0, 9};
        zeroFront(arr71);
        for (int i = 0; i < arr71.length; i++) {
            System.out.print(arr71[i]);
        }
        System.out.println(" ");

        //Task 8
        int[] arr81 = {0, 1, 2, 3, 4, 5, 6, 6, 7, 7, 8, 8, 9, 9};
        evenOdd(arr81);
        for (int i = 0; i < arr81.length; i++) {
            System.out.print(arr81[i]);
        }
        System.out.println(" ");

        //Task 9
        int[] arr91 = {1, 1, 1, 6, 1, 1, 7, 1, 6, 1, 1, 7, 1};
        System.out.println(sum67(arr91));

        //Task 10
        int[] arr101 = {1, 4, 5, 6, 2};
        int[] arr102 = {1, 2, 4};
        System.out.println(tripleUp(arr101));
        System.out.println(tripleUp(arr102));
    }

    public static String[] createArray(int numberOfElements) {
        String[] resultArray = new String[numberOfElements];
        if (numberOfElements > 0) {
            for (int i = 0; i < numberOfElements; i++) {
                resultArray[i] = Integer.toString(i);
            }
        }
        return resultArray;
    }

    public static Boolean either24(int arrayIntNumbers[]) {
        int twoPairs = 0, fourPairs = 0;
        for (int i = 1; i < arrayIntNumbers.length; i++) {
            if ((arrayIntNumbers[i - 1] == 2) && (arrayIntNumbers[i] == 2)) {
                twoPairs++;
            }
            if ((arrayIntNumbers[i - 1] == 4) && (arrayIntNumbers[i] == 4)) {
                fourPairs++;
            }
        }
        if ((twoPairs > 0) && (fourPairs > 0)) {
            return false;
        } else {
            return true;
        }
    }

    public static Boolean has12(int arrayIntNumbers[]) {
        int numberOne = 0;
        for (int i = 0; i < arrayIntNumbers.length; i++) {
            if (arrayIntNumbers[i] == 1) {
                numberOne++;
            }
            if ((arrayIntNumbers[i] == 2) && (numberOne > 0)) {
                return true;
            }
        }
        return false;
    }

    public static Boolean twoTwo(int arrayIntNumbers[]) {
        for (int i = 1; i < (arrayIntNumbers.length - 1); i++) {
            if ((arrayIntNumbers[i] == 7) && (arrayIntNumbers[i - 1] != 7) && (arrayIntNumbers[i + 1] != 7)) {
                return false;
            }
        }
        if ((arrayIntNumbers[(arrayIntNumbers.length - 1)] == 7) && (arrayIntNumbers[arrayIntNumbers.length - 2] != 7)) {
            return false;
        }
        return true;
    }

    public static int[] createArrayN(int start, int end) {
        if (start < end) {
            int[] arrayOfNumbers = new int[(end - start)];
            for (int i = start, j = 0; i < end; i++, j++) {
                arrayOfNumbers[j] = i;
            }
            return arrayOfNumbers;
        } else {
            System.out.println("Недопустимый диапазон!");
            int[] arrayOfNumbers = {};
            return arrayOfNumbers;
        }
    }

    public static int[] pre4(int arrayIntNumbers[]) {
        int fourPosition = 0;
        for (int i = 0; i < arrayIntNumbers.length; i++) {
            if (arrayIntNumbers[i] != 4) {
                fourPosition++;
            } else {
                break;
            }
        }
        int[] arrayOfNumbers = new int[fourPosition];
        for (int i = 0; i < fourPosition; i++) {
            arrayOfNumbers[i] = arrayIntNumbers[i];
        }
        return arrayOfNumbers;
    }

    public static int[] zeroFront(int arrayIntNumbers[]) {
        boolean change = true;
        while (change) {
            change = false;
            for (int i = 1; i < arrayIntNumbers.length; i++) {
                if ((arrayIntNumbers[i] == 0) && (arrayIntNumbers[i - 1] != 0)) {
                    int temp = 0;
                    temp = arrayIntNumbers[i - 1];
                    arrayIntNumbers[i - 1] = arrayIntNumbers[i];
                    arrayIntNumbers[i] = temp;
                    change = true;
                }
            }
        }
        return arrayIntNumbers;
    }

    public static int[] evenOdd(int arrayIntNumbers[]) {
        boolean change = true;
        while (change) {
            change = false;
            for (int i = 1; i < arrayIntNumbers.length; i++) {
                if ((arrayIntNumbers[i] % 2 == 0) && (arrayIntNumbers[i - 1] % 2 == 1)) {
                    int temp = 0;
                    temp = arrayIntNumbers[i - 1];
                    arrayIntNumbers[i - 1] = arrayIntNumbers[i];
                    arrayIntNumbers[i] = temp;
                    change = true;
                }
            }
        }
        return arrayIntNumbers;
    }

    public static int sum67(int arrayIntNumbers[]) {
        int sum = 0;
        for (int i = 0; i < arrayIntNumbers.length; i++) {
            if (arrayIntNumbers[i] == 6) {
                while (arrayIntNumbers[i] != 7) {
                    i++;
                }
            } else {
                sum += arrayIntNumbers[i];
            }
        }
        return sum;
    }

    public static Boolean tripleUp(int arrayIntNumbers[]) {
        for (int i = 0; i < (arrayIntNumbers.length - 2); i++) {
            if ((arrayIntNumbers[i] == (arrayIntNumbers[i + 1] - 1) && (arrayIntNumbers[i] == (arrayIntNumbers[i + 2] - 2)))) {
                return true;
            }
        }
        return false;
    }
}
