package CarConveyor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Conveyor {
    public static void main(String[] args) {
        ArrayList<Object> carList = new ArrayList();

        CarHonda honda1 = new CarHonda();
        CarHonda honda2 = new CarHonda();
        CarHonda honda3 = new CarHonda();

        CarToyota toyota1 = new CarToyota();
        CarToyota toyota2 = new CarToyota();
        CarToyota toyota3 = new CarToyota();

        CarZaz zaz1 = new CarZaz();
        CarZaz zaz2 = new CarZaz();
        CarZaz zaz3 = new CarZaz();

        carList.add(honda1);
        carList.add(honda2);
        carList.add(honda3);

        carList.add(toyota1);
        carList.add(toyota2);
        carList.add(toyota3);

        carList.add(zaz1);
        carList.add(zaz2);
        carList.add(zaz3);

        ArrayList<String> storeOfGearBoxes = new ArrayList<String>(Arrays.asList("A1A1", "B1B1", "C1C1", "A1A1", "B1B1", "C1C1", "A1A1", "B1B1", "C1C1"));
        ArrayList<String> storeOfEngines = new ArrayList<String>(Arrays.asList("AA1", "BB1", "CC1", "AA1", "BB1", "CC1", "AA1", "BB1", "CC1"));
        ArrayList<String> storeOfBodies = new ArrayList<String>(Arrays.asList("E1", "E2", "E3", "E1", "E2", "E3", "E1", "E2", "E3"));

        HashMap<String, ArrayList<String>> store = new HashMap<String, ArrayList<String>>();
        store.put("gearBox", storeOfGearBoxes);
        store.put("engine", storeOfEngines);
        store.put("body", storeOfBodies);

        for (int i = 0; i < carList.size(); i++) {
            if (assemble(carList.get(i), store)) {
                System.out.println("Car number " + i + " is assembled!");
            } else {
                System.out.println("Conveyor was stopped on car number " + i + " !");
                break;
            }
        }
    }

    private static boolean assemble(Object car, HashMap<String, ArrayList<String>> store) {
        Class classCar = car.getClass();
        try {
            Field[] carFields = classCar.getFields();
            boolean flag;
            for (Field field : carFields) {
                ArrayList<String> list = store.get(field.getName());
                Applyable applyable = field.getAnnotation(Applyable.class);
                flag = false;
                for (int i = 0; i < list.size(); i++) {
                    for (int j = 0; j < applyable.parts().length; j++) {
                        if (list.get(i).equals(applyable.parts()[j])) {
                            field.set(car, list.get(i));
                            list.remove(i);
                            flag = true;
                            break;
                        }
                    }
                    if (flag) {
                        break;
                    }
                }
                if (flag != true) {
                    System.out.println("There is not enough " + field.getName() + " for " + car.getClass().getSimpleName());
                    return false;
                }
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return true;
    }
}
