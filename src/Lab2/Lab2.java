package Lab2;

/**
 * Created by Admin on 10.10.14.
 */
public class Lab2 {
    public static void main(String[] args) {
        DealWithStrings testString = new DealWithStrings();

        System.out.println(testString.wrapWord("[[]]", "word"));

        System.out.println(testString.firstHalf("HelloThere"));

        System.out.println(testString.nonStart("Hello", "There"));

        System.out.println(testString.hasBad("badxx"));
        System.out.println(testString.hasBad("xxbadxx"));
        System.out.println(testString.hasBad("xbadxx"));

        System.out.println(testString.withoutX("xHix"));
        System.out.println(testString.withoutX("xHi"));
        System.out.println(testString.withoutX("Hxix"));

        System.out.println(testString.makeTags("cite", "Hello"));

        System.out.println(testString.atFirst("Hello"));
        System.out.println(testString.atFirst("He"));
        System.out.println(testString.atFirst("H"));
        System.out.println(testString.atFirst(""));

        System.out.println(testString.comboString("Hello", "hi"));

        System.out.println(testString.right2("Hello"));
        System.out.println(testString.right2("java"));
        System.out.println(testString.right2("Hi"));

        System.out.println(testString.minCat("Hello", "Hi"));
        System.out.println(testString.minCat("Hello", "java"));
        System.out.println(testString.minCat("java", "Hello"));
        System.out.println(testString.minCat("Java", "java"));
    }
}
