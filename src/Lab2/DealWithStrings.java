package Lab2;

/**
 * Created by Admin on 10.10.14.
 */
public class DealWithStrings {

    public String wrapWord(String wrapSymbols, String mainString) {
        return wrapSymbols.substring(0, 2) + mainString + wrapSymbols.substring(2);
    }

    public String firstHalf(String mainString) {
        return mainString.substring(0, (mainString.length() / 2));
    }

    public String nonStart(String firstWord, String secondWord) {
        return firstWord.substring(1) + secondWord.substring(1);
    }

    public boolean hasBad(String mainString) {
        if (mainString.length() > 2) {
            return "bad".equalsIgnoreCase(mainString.substring(0, 3));
        } else {
            return false;
        }
    }

    public String withoutX(String mainString) {
        if (mainString.startsWith("x") && mainString.endsWith("x")) {
            return mainString.substring(1, (mainString.length() - 1));
        } else if (mainString.endsWith("x")) {
            return mainString.substring(0, (mainString.length() - 1));
        } else if (mainString.startsWith("x")) {
            return mainString.substring(1);
        } else return mainString;
    }

    public String makeTags(String tag, String mainString) {
        return "<" + tag + ">" + mainString + "</" + tag + ">";
    }

    public String atFirst(String mainString) {
        if (mainString.length() > 1) {
            return mainString.substring(0, 2);
        } else if (mainString.length() == 1) {
            return mainString + "@";
        } else {
            return "@@";
        }
    }

    public String comboString(String firstString, String secondString) {
        if ((firstString.length() == 0) && (secondString.length() == 0)) {
            return firstString;
        } else if (firstString.length() > secondString.length()) {
            return secondString + firstString + secondString;
        } else {
            return firstString + secondString + firstString;
        }
    }

    public String right2(String mainString) {
        if (mainString.length() == 2) {
            return mainString;
        } else {
            return mainString.substring(mainString.length() - 2) + mainString.substring(0, (mainString.length() - 2));
        }
    }

    public String minCat(String firstString, String secondString) {
        if (firstString.length() > secondString.length()) {
            return firstString.substring(firstString.length() - secondString.length()) + secondString;
        } else if (firstString.length() < secondString.length()) {
            return firstString + secondString.substring(secondString.length() - firstString.length());
        } else {
            return firstString + secondString;
        }
    }
}
