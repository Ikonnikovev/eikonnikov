package Lab4;

/**
 * Created by Admin on 22.10.14.
 */
public class GreetingInEnglish implements Greeting {
    private String name;

    public GreetingInEnglish() {
        super();
        name = "User";
    }

    public GreetingInEnglish(String name) {
        super();
        this.name = name;
    }


    @Override
    public void getGreeting() {
        getGreetingWithName(name);
    }

    @Override
    public void getGreetingWithName(String name) {
        System.out.println("Hello dear " + name + " !");
    }
}
