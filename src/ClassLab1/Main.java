package ClassLab1;

import ClassLab1.Bike;
import ClassLab1.Car;
import ClassLab1.Model.Vehicle;
import ClassLab1.Truck;

/**
 * Created by user on 16.10.14.
 */
public class Main {
    public static void main(String[] args) {

        Vehicle vehicles [] = new Vehicle[10];
        vehicles[0] = new Truck();
        vehicles[1] = new Car();
        vehicles[2] = new Bike();
        vehicles[3] = new Truck();
        vehicles[4] = new Car();
        vehicles[5] = new Bike();
        vehicles[6] = new Truck();
        vehicles[7] = new Car();
        vehicles[8] = new Bike();
        vehicles[9] = new Truck();

        double longitude [] = {10,20,30,40,50,60,70,80,90,100};
        double latitude [] = {100,90,80,70,60,50,40,30,20,10};

        for (int i=0; i<vehicles.length; i++){
            vehicles[i].move(longitude[i], latitude[i]);
            vehicles[i].stop();
            if (vehicles[i] instanceof Truck){
                ((Truck)vehicles[i]).downLoad(10);
            } else if (vehicles[i] instanceof Car) {
                ((Car) vehicles[i]).addPassenger();
            } else {
                ((Bike) vehicles[i]).crush();
            }
        }

    }
}
