package StreamTest;

import java.io.*;

public class StreamTest {
    public static void main(String[] args) {
        File file = new File("e:/test.txt");
        File res = new File("e:/res.txt");
        String temporaryVariable;
        if (file.isFile()) {
            if (file.canRead()) {
                BufferedReader br = null;
                BufferedWriter bw = null;
                try {
                    br = new BufferedReader(new FileReader(file));
                    bw = new BufferedWriter(new FileWriter(res));
                    while ((temporaryVariable = br.readLine()) != null) {
                        if (temporaryVariable.contains("java")) {
                            bw.write(temporaryVariable+"\r\n");
                        }
                    }
                } catch (IOException exc) {
                    System.out.println("I/O Error: " + exc);
                } finally {
                    try {
                        br.close();
                        bw.flush();
                        bw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            } else {
                System.out.println("File cannot be read!");
            }
        } else {
            System.out.println("File was not found!");
        }
    }
}






