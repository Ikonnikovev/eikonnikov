package Lab1;

/**
 * Created by Admin on 08.10.14.
 */
public abstract class PowerPlant {
    protected String name;
    protected int electricalPower;
    protected int yearOfFoundation;
    protected String location;

    PowerPlant() {

    }

    PowerPlant(String name, int electricalPower, int yearOfFoundation, String location) {
        this.name = name;
        this.electricalPower = electricalPower;
        this.yearOfFoundation = yearOfFoundation;
        this.location = location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setElectricalPower(int electricalPower) {
        this.electricalPower = electricalPower;
    }

    public void setYearOfFoundation(int yearOfFoundation) {
        this.yearOfFoundation = yearOfFoundation;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public int getElectricalPower() {
        return electricalPower;
    }

    public int getYearOfFoundation() {
        return yearOfFoundation;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return this.name + " установленной мощностью " + this.electricalPower + " МВт\nбыла введена в эксплуатацию в "
                + this.yearOfFoundation + " году в городе " + this.location + ".\n";
    }

    protected abstract void showPrincipalTypeOfEquipment();

    protected abstract void showAnnualGrossElectricalOutput();
}
