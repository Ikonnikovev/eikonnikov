package ClassLab1;

import ClassLab1.Model.Vehicle;

/**
 * Created by user on 16.10.14.
 */
public class Truck extends Vehicle{
    private float longOfTruck;

    public Truck() {
    }

    public Truck(int height, int width, int length, double speed, int wheelCount, float longOfTruck) {
        super(height, width, length, speed, wheelCount);
        this.longOfTruck = longOfTruck;
    }

    public void downLoad (float weight){
        System.out.println(weight + " tons were downloaded");
    }

    public float getLongOfTruck() {
        return longOfTruck;
    }

    public void setLong(float longOfTruck) {
        this.longOfTruck = longOfTruck;
    }

    @Override
    public void aging(int years) {
        System.out.println("Truck got older on " + years + " years.");
    }

    @Override
    public void changePart(String partNumber) {
        System.out.println("Part " + partNumber + " is changed.");
    }

    @Override
    public void move(double x, double y) {
        System.out.println("New position from 0.0 is " + x + " . " + y);
    }

    @Override
    public void stop() {
        System.out.println("Truck was stopped.");
    }

    @Override
    public void refuel() {
        System.out.println("The tank is full.");
    }
}
