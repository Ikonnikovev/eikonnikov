package Lab4;

/**
 * Created by Admin on 22.10.14.
 */
public class GreetingInRussian implements Greeting {
    private String name;

    public GreetingInRussian() {
        super();
        name = "Пользователь";
    }

    public GreetingInRussian(String name) {
        super();
        this.name = name;
    }

    @Override
    public void getGreeting() {
        getGreetingWithName(name);
    }

    @Override
    public void getGreetingWithName(String name) {
        System.out.println("Здравствуйте, дорогой " + name + " !");
    }
}
