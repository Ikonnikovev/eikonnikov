package CarConveyor;

/**
 * Created by user on 25.11.14.
 */
public class CarHonda {
    @Applyable(parts = {"A1A1", "B1B1", "C1C1"})
    public String gearBox;
    @Applyable(parts = {"AA1", "BB1"})
    public String engine;
    @Applyable(parts = {"E1"})
    public String body;

    @Override
    public String toString (){
        return "Honda gearBox = "+gearBox;
    }
}
