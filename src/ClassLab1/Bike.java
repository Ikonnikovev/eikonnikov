package ClassLab1;

import ClassLab1.Model.Vehicle;

/**
 * Created by user on 16.10.14.
 */
public class Bike extends Vehicle {
    private int placeCount;

    public Bike() {
    }

    public Bike(int height, int width, int length, double speed, int wheelCount, int placeCount) {
        super(height, width, length, speed, wheelCount);
        this.placeCount = placeCount;
    }

    public void crush (){
        System.out.println("Bike is crushed");
    }

    public int getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(int placeCount) {
        this.placeCount = placeCount;
    }

    @Override
    public void aging(int years) {
        System.out.println("Bike got older on " + years + " years.");
    }

    @Override
    public void changePart(String partNumber) {
        System.out.println("Part " + partNumber + " is changed.");
    }

    @Override
    public void move(double x, double y) {
        System.out.println("New position from 0.0 is " + x + " . " + y);
    }

    @Override
    public void stop() {
        System.out.println("Bike was stopped.");
    }

    @Override
    public void refuel() {
        System.out.println("The tank is full.");
    }


}
