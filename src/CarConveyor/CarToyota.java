package CarConveyor;

/**
 * Created by user on 25.11.14.
 */
public class CarToyota {
    @Applyable(parts = {"B1B1", "C1C1"})
    public String gearBox;
    @Applyable(parts = {"AA1", "CC1"})
    public String engine;
    @Applyable(parts = {"E1", "E3"})
    public String body;

    @Override
    public String toString (){
        return "Toyota gearBox = "+gearBox;
    }
}
