package ReflectionTest;

/**
 * Created by Admin on 21.11.14.
 */
public class BookReader {
    public String firstName;
    public String lastName;
    public Integer age;
    Book book;

    public BookReader() {
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void readBook (String author, String name){
        Book book = new Book(author, name);
        this.book=book;
    }
}
