package LevelUp;

import LevelUp.Model.Vehicle;

/**
 * Created by user on 16.10.14.
 */
public class Car extends Vehicle {
    private int doorCount;

    public Car() {
    }

    public Car(int height, int width, int length, double speed, int wheelCount, int doorCount) {
        super(height, width, length, speed, wheelCount);
        this.doorCount = doorCount;
    }

    public void addPassenger (){
        System.out.println("One passenger was added.");
    }

    public int getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(int doorCount) {
        this.doorCount = doorCount;
    }

    @Override
    public void aging(int years) {
        System.out.println("Car got older on " + years + " years.");
    }

    @Override
    public void changePart(String partNumber) {
        System.out.println("Part " + partNumber + " is changed.");
    }

    @Override
    public void move(double x, double y) {
        System.out.println("New position from 0.0 is " + x + " . " + y);
    }

    @Override
    public void stop() {
        System.out.println("Car was stopped.");
    }

    @Override
    public void refuel() {
        System.out.println("The tank is full.");
    }
}
