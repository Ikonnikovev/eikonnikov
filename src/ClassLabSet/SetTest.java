package ClassLabSet;

import java.util.*;

public class SetTest {

    public static void main(String[] args) {
        ArrayList<Auto> collectionOfAuto = new ArrayList<Auto>();
        collectionOfAuto.add(new Auto("a", "green", "11"));
        collectionOfAuto.add(new Auto("b", "red", "22"));
        collectionOfAuto.add(new Auto("c", "yellow", "33"));
        collectionOfAuto.add(new Auto("d", "brown", "44"));
        collectionOfAuto.add(new Auto("e", "black", "55"));
        collectionOfAuto.add(new Auto("f", "black", "66"));
        collectionOfAuto.add(new Auto("g", "yellow", "77"));
        collectionOfAuto.add(new Auto("h", "red", "77"));

        System.out.println("Default order " + collectionOfAuto);

        Collections.sort(collectionOfAuto, new Comparator<Auto>() {
            @Override
            public int compare(Auto auto, Auto auto2) {
                int len1 = auto.color.length();
                int len2 = auto2.color.length();
                char v1[] = auto.color.toCharArray();
                char v2[] = auto2.color.toCharArray();
                int n = Math.min(len1, len2);
                int k = 0;
                while (k < n) {
                    if (v1[k] != v2[k]) {
                        return v1[k] - v2[k];
                    }
                    k++;
                }

                return len1 - len2;
            }
        });

        System.out.println("Sorted by name of color " + collectionOfAuto);

        TreeSet<Auto> treeSetOfAutoWithUniqueNumbers = new TreeSet<Auto>(new Comparator<Auto>() {
            @Override
            public int compare(Auto auto, Auto auto2) {
                if (auto.number==auto2.number){
                    return 0;
                }
                return 1;
            }
        } );

        for (int i=0; i<collectionOfAuto.size(); i++){
            treeSetOfAutoWithUniqueNumbers.add(collectionOfAuto.get(i));
        }

        System.out.println("Unique auto by number " + treeSetOfAutoWithUniqueNumbers);

        TreeSet<Auto> treeSetOfAutoWithUniqueColor = new TreeSet<Auto>(new Comparator<Auto>() {
            @Override
            public int compare(Auto auto, Auto auto2) {
                if (auto.color==auto2.color){
                    return 0;
                }
                return 1;
            }
        } );

        treeSetOfAutoWithUniqueColor.addAll(collectionOfAuto);

        System.out.println("Unique auto by color " + treeSetOfAutoWithUniqueColor);

        TreeSet<Auto> treeSetSortedByName = new TreeSet<Auto>(new Comparator<Auto>() {
            @Override
            public int compare(Auto auto, Auto auto2) {
                if (auto.name==auto2.name){
                    return 0;
                }
                return 1;
            }
        } );



        treeSetSortedByName.addAll(collectionOfAuto);

        System.out.println("Unique auto by name " + treeSetSortedByName);


    }


}
