package CopyDirectoryWithFilter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class CopyDirectoryWithFilter {

    public static void main(String[] args) {
        File givenDirectory = new File("E:/Test");
        File necessaryDirectory = new File("E:/Result");
        MyFilter myFilter = new MyFilter();
        CopyDirectoryWithFilter copyDirectoryWithFilter = new CopyDirectoryWithFilter();
        copyDirectoryWithFilter.copyFiles(givenDirectory, necessaryDirectory, myFilter, true);
    }

    public void copyFiles(File Directory, File necessaryDirectory, FilenameFilter MyFilter, boolean structure) {
        CopyDirectoryWithFilter.copy(Directory, necessaryDirectory, MyFilter, structure, Directory.getPath().length());
    }

    private static void copy(File Directory, File necessaryDirectory, FilenameFilter MyFilter, boolean structure, int pathLength) {
        File[] list = Directory.listFiles();
        for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
                copy(list[i], necessaryDirectory, MyFilter, structure, pathLength);
            } else {
                if (structure) {
                    if (MyFilter.accept(list[i], list[i].getName())) {
                        File temporaryForFolder = new File(necessaryDirectory.getPath() + "/"
                                + list[i].getParent().substring(pathLength));
                        temporaryForFolder.mkdirs();
                        File temporaryForFile = new File(necessaryDirectory.getPath() + "/"
                                + list[i].getPath().substring(pathLength));
                        try {
                            temporaryForFile.createNewFile();
                            Files.copy(list[i].toPath(), temporaryForFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (MyFilter.accept(list[i], list[i].getName())) {
                        File temporaryForFile = new File(necessaryDirectory.getPath() + "/"
                                + list[i].getName());
                        try {
                            temporaryForFile.createNewFile();
                            Files.copy(list[i].toPath(), temporaryForFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
