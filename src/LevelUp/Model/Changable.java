package LevelUp.Model;

/**
 * Created by user on 16.10.14.
 */
public interface Changable {
    void changePart (String partNumber);
}
