package Lab5;

/**
 * Created by Admin on 27.10.14.
 */
public class Lab5 {
    public static void main(String[] args) {
        System.out.println("-----Task #1------");
        System.out.println(Lab5.makeBricks(3, 1, 8));
        System.out.println(Lab5.makeBricks(3, 1, 9));
        System.out.println(Lab5.makeBricks(3, 2, 10));
        System.out.println(Lab5.makeBricks(3, 3, 17));
        System.out.println(Lab5.makeBricks(10, 2, 19));

        System.out.println("-----Task #2------");
        System.out.println(Lab5.noTeenSum(1, 2, 3));
        System.out.println(Lab5.noTeenSum(2, 13, 1));
        System.out.println(Lab5.noTeenSum(2, 1, 14));
        System.out.println(Lab5.noTeenSum(2, 1, 15));

        System.out.println("-----Task #3------");
        System.out.println(Lab5.blackjack(19, 21));
        System.out.println(Lab5.blackjack(21, 19));
        System.out.println(Lab5.blackjack(19, 22));
        System.out.println(Lab5.blackjack(22, 22));
        System.out.println(Lab5.blackjack(0, 0));


    }

    public static boolean makeBricks(int numberOfBricksLength1, int numberOfBricksLength5, int lengthOfBricks) {
        if (lengthOfBricks > 0) {
            if (((numberOfBricksLength1 + 5 * numberOfBricksLength5) >= lengthOfBricks) && (numberOfBricksLength1 >= (lengthOfBricks % 5))) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static int noTeenSum(int digitOne, int digitTwo, int digitThree) {
        return Lab5.fixTeen(digitOne) + Lab5.fixTeen(digitTwo) + Lab5.fixTeen(digitThree);
    }

    public static int fixTeen(int digit) {
        if (digit > 12 && digit < 20 && digit != 15 && digit != 16) {
            return 0;
        }
        return digit;
    }

    public static int blackjack(int firstCard, int secondCard) {
        if (firstCard < 1 || secondCard < 1) {
            System.out.println("Inappropriate numbers!");
            return 0;
        } else if (firstCard > 21 && secondCard > 21) {
            return 0;
        } else if (firstCard > 21) {
            return secondCard;
        } else if (secondCard > 21) {
            return firstCard;
        } else if (firstCard > secondCard) {
            return firstCard;
        }
        return secondCard;
    }
}
